# tutorial-arise-defects

## Running the notebook locally:

To install docker please follow the instructions provided [here](https://docs.docker.com/engine/install/).


```bash
docker run -it --rm -p 8888:8888 gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkit/tutorial-arise-defects
```
Then open the supplied link into a browser. 

