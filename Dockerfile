FROM jupyter/tensorflow-notebook:python-3.10


# ================================================================================
# Linux applications and libraries
# ================================================================================

# USER root
# RUN apt update --yes \
#  && apt install --yes --quiet --no-install-recommends \
#     git \
#  && apt clean \
#  && rm -rf /var/lib/apt/lists/*


# ================================================================================
# Python environment
# ================================================================================
# TODO: build fixed requirement.txt
# pip install --no-cache-dir 'git+https://github.com/angeloziletti/ai4materials.git' \
#     hdbscan \
#     umap-learn \
#  && fix-permissions "${CONDA_DIR}" \
#  && fix-permissions "/home/${NB_USER}"

WORKDIR /tmp/

COPY --chown=${NB_UID}:${NB_GID} ai4materials/ ./ai4materials
RUN pip install --no-cache-dir ./ai4materials \
 && pip install --no-cache-dir \
    hdbscan \
    umap-learn \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

# ================================================================================
# Switch back to jovyan to avoid accidental container runs as root
# ================================================================================

WORKDIR ${HOME}
USER ${NB_UID}
ENV DOCKER_STACKS_JUPYTER_CMD="nbclassic"

COPY --chown=${NB_UID}:${NB_GID} notebook/ ./notebook/
